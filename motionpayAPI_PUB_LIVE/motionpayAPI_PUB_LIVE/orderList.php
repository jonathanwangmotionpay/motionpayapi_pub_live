<?php
ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Data.php";
require_once "./lib/MotionPay.Api.php";
require_once './lib/Log.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Order List Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->


<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span>Sample Order List Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>      
      <span style="float:right;"><a href="php"><font style="font-size:15px;font-weight:bold;color:#f60;">Payment Page</font></a></span> 
    </div>
</div>
  
    <div class="bank_list accounts_pay" style="display:block;">
    
      <h6><label>Motion Pay Order List</label></h6>
  	  <ul class="sel_list">
<?php
        $filename = MotionPayConfig::getMotionPayCallbackFilename();
        // echo "filename is:" . $filename;
        
        if(file_exists($filename) == false) {
            echo "file doesn't exist:" . $filename . "\n";
        }
        else {
            echo "<table> \n";
            foreach(file($filename) as $line) {
                if(strlen($line) > 0) {
                    $pos = strpos($line, "out_trade_no");
                    if($pos > 0) {
                        $info = "[info]";
                        $pos = strpos($line,$info);
                        $jsonInlog = substr($line,$pos + strlen($info));
                        $jsonStr = trim($jsonInlog);
                        
                        // echo "jsonStr:" . $jsonStr;
                        $arrayResult = json_decode($jsonStr, true);
                        $out_trade_no = $arrayResult['out_trade_no'];
                        $pay_result = $arrayResult['pay_result'];
                        $total_fee = $arrayResult['total_fee'];
                        $mid = $arrayResult['mid'];
                        
                        if (strlen($out_trade_no) > 0) {
                            echo "<tr><td>\n";
                            echo "OrderId: <a href='orderDetail.php?out_trade_no=" . $out_trade_no . "&mid=" . $mid . "'>" . $out_trade_no . "</a>";
                            echo "</td></tr>\n";
                        }
                    }
                }
            }
            echo "</table> \n";
        }
?>
  	  </ul>
	</div>
</div>

</body>

</html>


