<?php
require_once "MotionPay.Config.php";
require_once "MotionPay.Exception.php";

/**
 * Data Class. All variables in the JSON Request and response
 * Use SHA1 to generate sign.
 * 数据对象基础类，该类中定义数据类最基本的行为，包括：
 * 计算/设置/获取签名、输出json格式的参数、从json读取数据对象等
 * @author Jonathan
 *
 */
class MotionPayDataBase
{
    protected $bodyValues = array();
    protected $merchantType = MotionPayConfig::ONLINE_MERCHANT;
    
    /**
     * set merchant type by matching merchant id.
     * 根据商户号，设置商户类型
     * @param string MerchantID
     */
    public function setMerchantTypeByMerchantId($mid) {
        if(strlen($mid) > 0) {
            if($mid == MotionPayConfig::getMid( MotionPayConfig::ONLINE_MERCHANT) ) {
                $this->merchantType = MotionPayConfig::ONLINE_MERCHANT;
            }
            if($mid == MotionPayConfig::getMid( MotionPayConfig::OFFLINE_MERCHANT) ) {
                $this->merchantType = MotionPayConfig::OFFLINE_MERCHANT;
            }
            if($mid == MotionPayConfig::getMid( MotionPayConfig::H5_MERCHANT) ) {
                $this->merchantType = MotionPayConfig::H5_MERCHANT;
            }
        }
    }
    
    
    /**
     * set merchant type
     * 设置商户类型
     **/
    public function setMerchantType($value) {
        $this->merchantType = $value;
    }
    
    /**
     * get merchant type
     * @return string merchanttype
     */
    /**
     *
     * 获取商户类型
     * @return string 商户类型
     **/
    public function getMerchantType() {
        return $this->merchantType;
    }
    
    /**
     * set merchantID
     * 设置商户号
     **/
    public function setMid()
    {
        $this->bodyValues['mid'] = MotionPayConfig::getMid($this->merchantType);
    }
    
    /**
     * get merchantID
     * @return string merchantID
     **/
    /**
     * 获取商户号
     * @return string 商户号
     **/
    public function getMid()
    {
        return $this->bodyValues['mid'];
    }
    
    /**
     * check if mid exists
     * @return true or false
     */
    /**
     * 判断商户号是否存在
     * @return true 或 false
     **/
    public function isMidSet()
    {
        return array_key_exists('mid', $this->bodyValues);
    }
    
    /**
     * set merchant order ID, it has to be unique for the same merchantID
     * @param string $value
     */
    /**
     * 设置商户支付订单号，同一商户唯一
     * @param string $value
     **/
    public function setOutTradeNo($value)
    {
        $this->bodyValues['out_trade_no'] = $value;
    }
    
    /**
     * get merchant order ID
     * @return string merchant order ID
     */
    /**
     * 获取商户支付订单号
     * @return string 商户支付订单号
     **/
    public function getOutTradeNo()
    {
        return $this->bodyValues['out_trade_no'];
    }
    
    /**
     * check if merchant order ID exists
     * @return true or false
     */
    /**
     * 判断商户支付订单号是否存在
     * @return true 或 false
     **/
    public function isOutTradeNoSet()
    {
        return array_key_exists('out_trade_no', $this->bodyValues);
    }
    
    /**
     * set the sign
     */
    /**
     * 设置签名，详见签名生成算法
     **/
    public function setSign($log = NULL)
    {
        $this->setMid();
        $this->bodyValues['sign'] = "";
        $sign = $this->makeSign($log);
        $this->bodyValues['sign'] = $sign;
        return $sign;
    }
    
    /**
     * get the sign
     * @return string sign
     */
    /**
     * 获取签名，详见签名生成算法的值
     * @return string 签名
     **/
    public function getSign()
    {
        return $this->bodyValues['sign'];
    }
    
    /**
     * check if the sign exists
     * @return true or false
     */
    /**
     * 判断签名，详见签名生成算法是否存在
     * @return true 或 false
     **/
    public function isSignSet()
    {
        return array_key_exists('sign', $this->bodyValues);
    }
    
    /**
     * Format the parameters before sign
     */
    /**
     * 格式化参数格式化成url参数
     */
    public function toQueryParams()
    {
        $buff = "";
        foreach ($this->bodyValues as $k => $v) {
            if ($v != "" && !is_array($v)) {
                $buff .= strtolower($k) . "=" . $v . "&";
            }
        }
        $buff = trim($buff, "&");
        return $buff;
    }
    
    /**
     * encode the values into JSON format
     */
    /**
     * 格式化参数格式化成json参数
     */
    public function toBodyParams()
    {
        return json_encode($this->bodyValues);
    }
    
    /**
     * format the sign parameters
     */
    /**
     * 格式化签名参数
     */
    public function toSignParams()
    {
        $buff = "";
        $buff .= "appid=" . MotionPayConfig::getAppID($this->merchantType) . "&appsecret=" . MotionPayConfig::getAPI_PASSWORD($this->merchantType);
        return $buff;
    }
    
    /**
     * generate the sign, please use setsign to set the return value to sign parameter
     * @return string sign
     */
    /**
     * 生成签名
     * @return 签名，本函数不覆盖sign成员变量，如要设置签名需要调用setSign方法赋值
     */
    public function makeSignWithAppIdAndSecret($appId, $appSecret)
    {
        //签名步骤一：获得签名ID与密码
        ksort($this->bodyValues);
        $string = "appid=" . $appId . "&appsecret=" . $appSecret;
        $queryParams = $this->toQueryParams();
        
        $stringToEncode = ($queryParams) . "&" . $string;
	// printf("in makeSignWithAppIdAndSecret stringToEncode is:" . $stringToEncode);
        
        $string = sha1(utf8_encode($stringToEncode),TRUE);
        $result = bin2hex($string);
        $result = strtoupper($result);
        return $result;
    }

    public function makeSignWithAppIdAndSecretMid($mid, $appId, $appSecret)
    {
	$this->bodyValues['mid'] = $mid;
        //签名步骤一：获得签名ID与密码
        ksort($this->bodyValues);
        $string = "appid=" . $appId . "&appsecret=" . $appSecret;
        $queryParams = $this->toQueryParams();
        
        $stringToEncode = ($queryParams) . "&" . $string;
	printf("in makeSignWithAppIdAndSecret stringToEncode is:" . $stringToEncode);
        
        $string = sha1(utf8_encode($stringToEncode),TRUE);
        $result = bin2hex($string);
        $result = strtoupper($result);
        return $result;
    }
    
    /**
     * generate the sign, please use setsign to set the return value to sign parameter
     * @return string sign
     */
    /**
     * 生成签名
     * @return 签名，本函数不覆盖sign成员变量，如要设置签名需要调用setSign方法赋值
     */
    public function makeSign($log = NULL)
    {
        //签名步骤一：获得签名ID与密码
        ksort($this->bodyValues);
        $string = $this->toSignParams();
        //签名步骤二：获得所以参数
        $queryParams = $this->toQueryParams();
        
        $stringToEncode = ($queryParams) . "&" . $string;
        if(is_null($log) == false)
        {
            $log->INFO("in makeSign stringToEncode is:" . $stringToEncode);
        }
        
        // echo "stringToEncode is:" . $stringToEncode;
        // $stringToEncode = 'goods_info=test_product&mid=100105100000011&out_trade_no=201712142891735269455&pay_channel=w&return_url=https://demo.motionpay.org/motionpayapi_java_pub/callbackservlet&spbill_create_ip=192.168.1.1&terminal_no=webserver&total_fee=1&appid=5005642017006&appsecret=2ce9f51261e21ba6a087ee239160f0b1';
        // $stringToEncode = 'goods_info=Test_Product&mid=100105100000011&out_trade_no=201712142891735269455&pay_channel=W&return_url=https://demo.motionpay.org/motionpayAPI_JAVA_PUB/CallbackServlet&spbill_create_ip=192.168.1.1&terminal_no=WebServer&total_fee=1&appid=5005642017006&appsecret=2ce9f51261e21ba6a087ee239160f0b1';
        //签名步骤三：SHA1加密
        $string = sha1(utf8_encode($stringToEncode),TRUE);
        $result = bin2hex($string);
        // echo "hash size:" . strlen($string);
        // echo "hash result:" . $string . "#";
        // echo "hex result:" . $result . "#";
        
        
        /*
         $totalbytes = strlen($string);
         for($byteposition = 0; $byteposition < $totalbytes; $byteposition++)
         {
         $currentbyte = $string[$byteposition];
         echo dechex(bindec($byteposition));
         var_dump($currentbyte);
         printf("!%d!%02X!", $byteposition, bin2hex($currentbyte));
         $offset = 0;
         printf("#%d#%02X#", $byteposition, ($this->ordutf8($currentbyte, $offset)));
         }
         */
        
        // $string = bin2hex($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($result);
        return $result;
    }
    
    /**
     * get body parameters value
     */
    /**
     * 获取设置的body参数值
     */
    public function getBodyValues()
    {
        return $this->bodyValues;
    }
    
    /**
     * convert the value for exchange_rate to string from float
     * 检查汇率是否是数字类型，如果是，将其转化为字符串。
     * @param callback JSON str $arrayStr
     */
    public function convertExchageRateToString($arrayStr, $log = NULL)
    {
        $str_result = $arrayStr;
        $pos = stripos($arrayStr, "exchange_rate");
        if(is_null($log) == false)
        {
            $log->INFO("pos is:" . $pos);
        }
        
        if($pos !== false)
        {
            $pos1 = stripos($arrayStr, ":", $pos);
            $pos2 = stripos($arrayStr, ",", $pos);
            if(is_null($log) == false)
            {
                $log->INFO("pos1 is:" . $pos1);
                $log->INFO("pos2 is:" . $pos2);
            }
            if ($pos1 !== false && $pos2 !== false && $pos2 > $pos1)
            {
                $pos1 = $pos1 + 1;
                $theRate = substr($arrayStr, $pos1, $pos2 - $pos1);
                $posQuota = stripos($theRate, '"');
                if ($posQuota === false)
                {
                    $str_result = substr($arrayStr, 0, $pos1) . '"' . substr($arrayStr, $pos1, $pos2 - $pos1) . '"' . substr($arrayStr, $pos2);
                }
            }
        }
        return $str_result;
    }
    
    /**
     * decode the JSON string to an array
     * @param array $array
     */
    /**
     *
     * 使用数组初始化
     * @param array $array
     */
    public function fromArray($array, $log = NULL)
    {
        $array = $this->convertExchageRateToString($array);
        $this->bodyValues =  json_decode($array, true);
    }
}

/**
 * Response Result from API
 * 接口调用结果类
 * @author Jonathan
 *
 */
class MotionPayResults extends MotionPayDataBase
{
    
    /**
     * Decode JSON string to array into bodyvalues
     * 使用数组初始化
     * @param array $array
     */
    public function fromArray($array)
    {
        $this->bodyValues = json_decode($array, true);
    }
    
    /**
     * init result object from JSON response
     * 将json转为array
     * @param string $json
     * @throws MotionPayException
     *
     */
    public static function init($array)
    {
        $obj = new self();
        $obj->fromArray($array);
        /*
         $content = $obj->bodyValues['content'];
         if(count($content) > 0) {
         $obj->bodyValues = array_merge($obj->bodyValues, $content);
         } */
        return $obj->getBodyValues();
    }
}

/**
 * Order Object
 * 统一下单对象
 * @author Jonathan
 */
class MotionPayOrder extends MotionPayDataBase
{
    /**
     * set pay channel, 'W' for wechat, 'A' for Alipay
     * 设置支付通道 'W'表示微信, 'A'表示支付宝
     * @param string $value
     **/
    public function setPayChannel($value)
    {
        $this->bodyValues['pay_channel'] = $value;
    }
    
    /**
     * get pay channel
     * 获取支付通道
     * @return string pay channel
     **/
    public function getPayChannel()
    {
        return $this->bodyValues['pay_channel'];
    }
    
    /**
     * check if pay channel exists
     * 判断支付通道是否存在
     * @return true or false
     **/
    public function isPayChannelSet()
    {
        return array_key_exists('pay_channel', $this->bodyValues);
    }
    
    /**
     * set the terminal No, It can be any value. We use "webserver" as a default value.
     * 设置终端代码 可以是任意终端名称，我们使用 "webserver" 作为缺省值
     * @param string $value
     **/
    public function setTerminalNo($value)
    {
        $this->bodyValues['terminal_no'] = $value;
    }
    
    /**
     * get terminal no
     * 获取终端代码
     * @return string no
     **/
    public function getTerminalNo()
    {
        return $this->bodyValues['terminal_no'];
    }
    
    /**
     * check if terminao no exists
     * 判断终端代码是否存在
     * @return true or false
     **/
    public function isTerminalNoSet()
    {
        return array_key_exists('terminal_no', $this->bodyValues);
    }
    
    
    /**
     * set terminal IP address
     * 设置终端IP
     **/
    public function setSpbillCreateIP()
    {
        $this->bodyValues['spbill_create_ip'] = MotionPayConfig::SPBILL_CREATE_IP;
    }
    
    /**
     * get terminal IP address
     * @return string terminal IP
     **/
    public function getSpbillCreateIP()
    {
        return $this->bodyValues['spbill_create_ip'];
    }
    
    /**
     * check if terminal IP exists
     * @return true or false
     **/
    public function isSpbillCreateIPSet()
    {
        return array_key_exists('spbill_create_ip', $this->bodyValues);
    }
    
    /**
     * set WAP_URL
     * @param string $value
     **/
    public function setWapURL($value)
    {
        $this->bodyValues['wap_url'] = $value;
    }
    
    /**
     * get WAP_URL
     * @return string WAP_URL
     **/
    public function getWapURL()
    {
        return $this->bodyValues['wap_url'];
    }
    
    
    
    /**
     * set goods info
     * @param string $value
     **/
    public function setGoodsInfo($value)
    {
        $value = str_replace(" ", "_", $value);
        $this->bodyValues['goods_info'] = $value;
    }
    
    /**
     * get goods info
     * @return string goods info
     **/
    public function getGoodsInfo()
    {
        return $this->bodyValues['goods_info'];
    }
    
    /**
     * check if goods info exists
     * @return true or false
     **/
    public function isGoodsInfoSet()
    {
        return array_key_exists('goods_info', $this->bodyValues);
    }
    
    /**
     * set total fee to charge the user. The value is in cents
     * 设置金额，单位为货币最小单位(cents)
     * @param string $value
     **/
    public function setTotalFee($value)
    {
        $this->bodyValues['total_fee'] = $value;
    }
    
    /**
     * set total fee
     * 获取金额，单位为货币最小单位
     * @return string total fee
     **/
    public function getTotalFee()
    {
        return $this->bodyValues['total_fee'];
    }
    
    /**
     * check if total fee exists
     * 判断金额是否存在
     * @return true 或 false
     **/
    public function isTotalFeeSet()
    {
        return array_key_exists('total_fee', $this->bodyValues);
    }
    
    /**
     * set notify URL
     * 设置支付异步通知url,不填则不会推送支付异步通知
     * @param string $value
     **/
    public function setNotifyUrl($value)
    {
        $this->bodyValues['return_url'] = $value;
    }
    
    /**
     * get notify url
     * 获取支付通知url
     * @return string notify url
     **/
    public function getNotifyUrl()
    {
        return $this->bodyValues['return_url'];
    }
    
    /**
     * check if notify url exists
     * 判断支付通知url是否存在
     * @return true or  false
     **/
    public function isNotifyUrlSet()
    {
        return array_key_exists('return_url', $this->bodyValues);
    }
}

/**
 * order query object. It is empty for now.
 * 查询订单状态对象. 目前为空
 * @author Jonathan
 */
class MotionPayOrderQuery extends MotionPayDataBase
{
}

/**
 * refund order object.
 * 申请退款对象
 * @author Jonathan
 */
class MotionPayRefund extends MotionPayDataBase
{
    
    /**
     * set total fee was charged for this order
     * 设置金额，单位为货币最小单位(cents)
     * @param string $value
     **/
    public function setTotalFee($value)
    {
        $this->bodyValues['total_fee'] = $value;
    }
    
    /**
     * get total fee
     * 获取金额，单位为货币最小单位
     * @return string total fee
     **/
    public function getTotalFee()
    {
        return $this->bodyValues['total_fee'];
    }
    
    /**
     * check if total fee exists
     * 判断金额是否存在
     * @return true or false
     **/
    public function isTotalFeeSet()
    {
        return array_key_exists('total_fee', $this->bodyValues);
    }
    
    /**
     * set refund amount
     * 设置退款金额，单位是货币最小单位
     * @param string $value
     **/
    public function setRefundAmount($value)
    {
        $this->bodyValues['refund_amount'] = $value;
    }
    
    /**
     * get refund amount
     * 获取退款金额
     * @return string $value
     **/
    public function getRefundAmount()
    {
        return $this->bodyValues['refund_amount'];
    }
    
    /**
     * check if refund amount exist
     * 判断退款金额是否存在
     * @return true or false
     **/
    public function isRefundAmountSet()
    {
        return array_key_exists('refund_amount', $this->bodyValues);
    }
}

