<?php
session_start();
ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Data.php";
require_once "./lib/MotionPay.Api.php";
require_once './lib/Log.php';


$WECHAT_OFFICAL_ACCOUNT_APPID = ("wx8ddacf4e1347f9c8");
$WECHAT_OFFICAL_ACCOUNT_APPSECURE = ("161abc59bf799fe3dbd730772ebf63ac");
$WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_MID = ("100100010000055");
$WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPID = ("5005642018019");
$WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPSECURE = ("c7d79e0075076e72c5f779fc62f0f4e9");
$ORDER_OFFICAL_ACCOUNT_URL = "https://online.motionpaytech.com/onlinePayment/v1_1/pay/jsapi";

$logHandler = new CLogFileHandler(MotionPayConfig::getMotionPayLogFilename());
$log = Log::Init($logHandler, 15);


/**
 * Work flow:
 * 1. Get the input parameters as a string from the JavaScript ajaxPost method.
 * 2. Parse the string to get the openid and amount parameter.
 * 3. Generate a random merchant order number
 * 4. Put all the request parameters and values into array. Generate the sign by appId and appSecure. 
 *    Please refer the document for more detail. 
 * 5. Send the JSON request to the Motion Pay JSON API and send the response back the ajaxPost method.
 */
/**
 * 流程：
 * 1、获取从 ajaxPost 方法发送来的字符串。
 * 2、解析字符串，获得 openid 和 amount 参数。
 * 3、生产随机数作为商户订单号。
 * 4、将所有参数和值放到数组中，生产签名。具体方法见API文档
 * 5、安装发送所有参数到 Motion Pay JSON API 接口，并将结果返回 ajaxPost 方法。
 */
$inputStr =  file_get_contents('php://input');
$log->INFO("the inputStr from page is:" . $inputStr);
parse_str($inputStr, $inputParams);
$total_fee = $inputParams['amount'];
$openid = $inputParams['openid'];
$log->INFO("the openid from page is:" . $openid);

$orderId = MotionPayApi::getNonceStr(10);
$outTradeNo = date("YmdHis") . $orderId;
$return_url = MotionPayConfig::getCallbackURL();  // replace it with your call back url or use the default one.

$requestArray = array();
$requestArray['mid'] = $WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_MID;
$requestArray['out_trade_no'] = $outTradeNo;
$requestArray['pay_channel'] = "W";
$requestArray['total_fee'] = $total_fee;
$requestArray['goods_info'] = "ThirdPartyProduct";
$requestArray['return_url'] = $return_url;
$requestArray['spbill_create_ip'] = "192.168.1.132";
// $result['terminal_no'] = "OfficalAccount"); // use OfficialAccount for next version
$requestArray['terminal_no'] = "WebServer"; // Use WebServer for now.
$requestArray['openid'] = $openid;

$dataForSign = new MotionPayDataBase();
$dataForSign->fromArray(json_encode($requestArray));
$signLocal = $dataForSign->makeSignWithAppIdAndSecret($WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPID,$WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPSECURE);

$requestArray['sign'] = $signLocal;

$requestJSON = json_encode($requestArray);

$log->INFO("requestJSON is:" . $requestJSON);


// Setup cURL
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $ORDER_OFFICAL_ACCOUNT_URL);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//set header
//设置header
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJSON);

//use curl to post to url
//运行curl
$responseStr = curl_exec($ch);
$log->INFO("json response string is:" . $responseStr);
curl_close($ch);

// Check for errors
if($responseStr === FALSE){
    die(curl_error($ch));
}

$log->INFO("responseStr is:" . $responseStr);

echo $responseStr;
?>    