<?php
// use this file to send a notify to the callback.php for testing.
// 用于发送一个 json request给 callback.php来测试 ballback.php 程序。

ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Api.php";
require_once "./lib/Log.php";

// Send a call back notify for testing
$jsonStr = '{"mid":"100105100000011","out_trade_no":"201802052288075889668","pay_channel":"A","pay_result":"SUCCESS","sign":"F3B655052E2CE6F02AC5BB107F828A8663017DF2","third_order_no":"2018020521001004120521241666","total_fee":1,"transaction_id":"01100105100000011201802050001","user_identify":"2088002664753126"}';

$url = MotionPayConfig::getCallbackURL();

echo "the url is:" . $url . "\n";
// Setup cURL
$ch = curl_init($url);
curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
    ),
    CURLOPT_SSL_VERIFYPEER => FALSE, 
    CURLOPT_POSTFIELDS => $jsonStr
));

// Send the request
$response = curl_exec($ch);

// Check for errors
if($response === FALSE){
    echo "errors:";
    die(curl_error($ch));
}

echo "success!";
// Print the date from the response
echo $response;
