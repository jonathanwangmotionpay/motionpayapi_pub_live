<?php
session_start();
ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Data.php";
require_once "./lib/MotionPay.Api.php";
require_once './lib/Log.php';


$WECHAT_OFFICAL_ACCOUNT_APPID = ("wx8ddacf4e1347f9c8");
$WECHAT_OFFICAL_ACCOUNT_APPSECURE = ("161abc59bf799fe3dbd730772ebf63ac");
$WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_MID = ("100100010000055");
$WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPID = ("5005642018019");
$WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_APPSECURE = ("c7d79e0075076e72c5f779fc62f0f4e9"); 


$logHandler = new CLogFileHandler(MotionPayConfig::getMotionPayLogFilename());
$log = Log::Init($logHandler, 15);


/**
 * Work flow:
 * 1. Check if we already have "openId" in the session. If we already have it, redirect to the subMerchantPay.php page. 
 *    If we don't have it, redirect to wechat server to get it. 
 *    After we get the openId from wechat server, wechat server will redirect the page back to this page. This page will make sure
 *    we got the "openId" in the session then redirect to subMerchantPay.php page. 
 * 2. For more detail about how to get the "openId" from wechat server. Please check wechat document about it.
 * 3. There is a cancel interface to cancel the payment request so there won't be any duplicated payments. It is not required.
 * 4. The callback parameter is to get the payment result notify. It is the same as QR code payment.
 */
/**
 * 流程：
 * 1、查看 session 中是否有已经有 "openId"，如果有，重定向页面到subMerchantPay.php。如果没有，重定向到 wechat服务器，以获取 openId。Wechat服务器会重定向页面回到该页面。
 *   回到这个页面之后，我们会确认已经获得 "openId"，然后再重定向页面到subMerchantPay.php. 
 * 2、需要了解 如何获得 "openId"的详情，请参考 wechat提供的文档。
 * 3、您可以使用 cancel 接口来取消支付请求，以避免同一个订单被多次支付。这不是必须要做的。
 * 4、您可以通过 callback 参数来获得 支付结果的异步通知。这部分功能与 QR code 支付方式是一样的。
 */

$code = $_GET['code'];
$log->INFO("code in request is:" . $code);
$appId = $WECHAT_OFFICAL_ACCOUNT_APPID;
$appSecret = $WECHAT_OFFICAL_ACCOUNT_APPSECURE;
// if code is not empty, it means wechat server redirected the page back to us. We just need to use this code to send a request to 
// the specified wechat URL to get the openId in the JSON response.
if(empty($code) == false) {
    $_SESSION["code"] = $code;
    $postURL = "https://api.weixin.qq.com/sns/oauth2/access_token?grant_type=authorization_code&appid=" . $appId . "&secret=" . $appSecret . "&code=" . $code;
    $log->INFO("postURL is:" . $postURL);
    $contentFromURL = file_get_contents($postURL);
    $log->INFO("contentFromURL is:" . $contentFromURL);
    $json = json_decode($contentFromURL, true);
    $openIdInJSON = $json['openid'];
    $log->INFO("openIdInJSON is:" . $openIdInJSON);
    // if openid in the json response is not empty. It means we have got the openid successfully.
    if(empty($openIdInJSON) == false) {
        $_SESSION["openId"] = $openIdInJSON;
    }
    else {
        // something is wrong. we didn't get the openid.
        echo "Failed to get the openId. Please check the log for more detail.";
        exit();
    }
}

$openId = $_SESSION["openId"];
$log->INFO("openId in session is:" . $openId);

if(empty($openId) == false) {
    // we have got the openid, we just need to redirect the page to the html page so user can input the price and click on pay now button
    // for the payment.
    $url2 = "subMerchantPay.php?openId=" . $openId . "&appId=" . $appId;
    header("Location: " . $url2);
    exit();
}
else {
    // openid is empty, redirect to the URL to get the code first.
    $log->INFO("openId in session is empty. Redirect to wechat server to get the openid.");
    $WX_RED_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={APPID}&redirect_uri={SCHEME}%3a%2f%2f{DNS}%2f{APPFOLDER}%2f{ADDR}&response_type=code&scope=snsapi_base&state={STATE}#wechat_redirect";
    
    $appId = $WECHAT_OFFICAL_ACCOUNT_APPID;
    $mid = $WECHAT_OFFICAL_ACCOUNT_MOTIONPAY_MID;
    $scheme = "https";
    $dns = $_SERVER['HTTP_HOST'];
    $appFolder = "motionpayAPI_PUB_LIVE";
    $addr = "subMerchantPayService.php";

    //Redirect to Wechat Server to get the openId. After that WeChat server will redirect it back to subMerchantPayService.php
    $redirectUrl = $WX_RED_URL;
    $redirectUrl = str_replace("{APPID}", $appId, $redirectUrl);
    $redirectUrl = str_replace("{SCHEME}", $scheme, $redirectUrl);
    $redirectUrl = str_replace("{DNS}", $dns, $redirectUrl);
    $redirectUrl = str_replace("{APPFOLDER}", $appFolder, $redirectUrl);
    $redirectUrl = str_replace("{ADDR}", $addr, $redirectUrl);
    $redirectUrl = str_replace("{STATE}", $mid, $redirectUrl);
    $log->INFO("redirectUrl is:" + $redirectUrl);
    header("Location: " . $redirectUrl);
    exit();
}
?>    
