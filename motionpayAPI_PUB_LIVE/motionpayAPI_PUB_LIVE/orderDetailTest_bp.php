<?php
ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Data.php";
require_once "./lib/MotionPay.Api.php";
require_once './lib/Log.php';

// MotionPayConfig::$USING_LIVE_APISERVER = true;
// MotionPayConfig::setLiveMerchantInfo("MotionPayConfig::ONLINE_MERCHANT", "100100030000563", "5005642018009", "0f68a426ba30006c4ccebed822306f97");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  	<title>Motion Pay Sample Order List Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
	
	
	<!-- BOOTSTRAP CSS and JS. Could be replaced by hosted BOOTSTRAP-4 after download ------> 
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
	
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<!-- BOOTSTRAP CSS and JS. Could be replaced by hosted BOOTSTRAP-4 after download ------>
	

  </head>

<body>
<div class="container">

<!-- header -->
<center>
<div id="header" class="w-50">
	  <div class="text-left">
	    <a href="#"><img src="images/Motionpay-Logo.gif"></img></a>	    
	  	<a href="http://motionpay.ca/"  class="float-right text-mute"><br/><small>Tech Support</small></a>
	  </div>	
	  
	  <hr class="table-bordered">
</div>
</center>
<!--header-->  


<!--content-->
<center>
<div name="outerbox" class="w-50 table-bordered">
	
	<div name="toptable" style="background:#edffcd;">
			<br/>
			<span class="float-left">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Sample Order Detail Page by 
				<span class="text-primary">
						Motion Pay</span>
			</span>
			
	  	<strong class="float-right">
	  		<a href="orderList_bp.php">
	  				<u><span class="text-warning">Order List</span></u></a>
	  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  	</strong>
	  	<br/>&nbsp;
  </div> <!-- end div-toptabel --->
  
  <div name="formtable">
		<br/>
		<div class="w-75  text-left">
		    <h6>
		    	Motion Pay Order Detail Sample Page
		    </h6>
  		
		    
    	    <form id="payment" action="orderRefund_bp.php" method="post" >
    	    <ul class="list-group">
    	    	<?php 
                    // $out_trade_no = "5607603";
                    // $mid = "100100030000563";
    	    	    $out_trade_no = "201803300048359754290801";
    	    	    $mid = "100100010000025";
    	    	
                    if(isset($_GET['out_trade_no'])) {
                        $out_trade_no = $_GET['out_trade_no'];
                    }
                    if(isset($_GET['mid'])) {
                        $mid = $_GET['mid'];
                    }
                    
                    if(strlen($out_trade_no) > 0 && strlen($mid) > 0) {
                        $message = "";
                        $input = new MotionPayOrderQuery();
                        $input->setMerchantTypeByMerchantId($mid);
                        $input->setOutTradeNo($out_trade_no);
                        $resultReturn = MotionPayApi::orderQuery($input);
                        if ($resultReturn['code'] == '0') {
                            $result = $resultReturn['content'];
                        }
                        else {
                            $message = $resultReturn['message'];
                        }
                        
                        if(strlen($message) > 0) {
                            echo "error:" . $message;
                        }
                        else {
                            // var_dump($result);
                    
                            $total_fee = $result["total_fee"];
                            $amountPaid = $total_fee * 1.0 / 100;
                            echo ("<li class='list-group-item'>total_fee: $" . $amountPaid . "</li>\n");
                            echo ("<li class='list-group-item'>pay_channel: " . $result["pay_channel"] . "</li>\n");
                            echo ("<li class='list-group-item'>trade_status: " . $result["trade_status"] . "</td></tr>\n");
                            echo ("<li class='list-group-item'>user_identify: " . $result["user_identify"] . "</td></tr>\n");
                            echo ("<li class='list-group-item'>Refund Amount:  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \n");
                            echo ("<input type='text' name='refundAmount' value='" . $amountPaid . "' size='5'/> &nbsp;\n");
                            echo ("<input type='hidden' name='totalAmount' value='" . $amountPaid . "'>\n");
                            echo ("<input type='hidden' name='out_trade_no' value='" . $out_trade_no . "'>\n");
                            echo ("<input type='hidden' name='mid' value='" . $mid . "'>\n");
                            echo ("<input type='submit' name='refund' value='Refund'></li>\n");
                            
                        }
                    }
                    else {
                        echo "out_trade_no or mid is empty.\n";
                    }
                    ?>
    	    </ul>
    		</form>
		    
  	</div> <!-- end div-formtable --->
	
</div> <!-- end div-outbox --->
</center>

</div> <!-- end div-container -->
</body>

</html>
  
